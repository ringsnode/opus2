These tests run against the production webserver api endpoints.

THe list of urls and expected result counts are found in result_counts.txt


Run the Tests: 

    python api_tests.py 


Install the requirements in this directory: 

    virtualenv --python=/usr/local/bin/python2.7 venv --distribute
    source venv/bin/activate    
    pip install -r requirements.txt



• tests in result_counts.text were originally exported as csv from this google doc: these were originally exported as csv from [this google doc](https://docs.google.com/spreadsheets/d/1zy3FX0vCGqAnm6vidZI9HJyEzatW3q57HQn0ef9Fsso/edit?usp=sharing)


